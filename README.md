# eff-presentation-ansible

## Clés SSH

### Sur les machines à déployer

> Si vous voulez ajouter les clés sur l'utilisateur root, vous devez autoriser la connexion par mot de passe sur l'utilisateur root dans la configuration SSH de cette manière:

```sh
sed -i 's/^\(PermitRootLogin \).*/\1Yes/' /etc/ssh/sshd_config && systemctl restart sshd
```

### Sur le master

```sh
ssh-keygen -f nomDeMaCle
ssh-copy-id -i nomDeMaCle.pub user@x.x.x.x
```

## Démarrer l'agent SSH

```sh
eval $(ssh-agent)
ssh-add chemin/vers/cle_prive
```