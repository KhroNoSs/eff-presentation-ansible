---
marp: true
theme: gaia
paginate: true
footer: 'Hugo Nowacki'
---

# Ansible et Infrastructure As Code

![bg 60% right:33%](https://upload.wikimedia.org/wikipedia/commons/2/24/Ansible_logo.svg)

1. Infrastructure As Code
2. Introduction Ansible
2. Bonnes pratiques Ansible
3. Pour aller plus loin

---

## Infrastructure As Code ?

* Décrire notre infrastructure dans des fichiers textes
* Exécuter/Interpréter ses fichiers textes avec des logiciels (Ansible, Terraform, Bash...)

![bg vertical 100% right:50%](https://learn.microsoft.com/fr-fr/devops/_img/infrastructureascode_600x300-3.png)
![bg vertical 100% right:50%](https://www.tinfoilcipher.co.uk/wp-content/uploads/2020/04/1-1024x190.jpg)

---

## Infrastructure as code - Les avantages ?

* Avoir une infrastructure **re déployables quasi instantanément**
* Nos fichiers font office de documentation.
* Moins **d'erreurs humaines**.

---

## C'est quoi Ansible ?

* Gestion de configuration
* Idempotence : Ansible détecte quand une tâche a déjà été faite
* Permet d'automatiser beaucoup de systèmes :
  * Linux, Unix, Windows
  * Équipements réseaux (Cisco, Fortinet...)
  * Ressources cloud (AWS, GCP, Azure, Scaleway...)
* Peu de prérequis nécessaires

---

## Ansible : prérequis

* Pas besoin d'agent à installer sur les hôtes (Puppet, Chef, Salt)
* Fonctionne via SSH : déployer une clé SSH sur les machines
* Python sur la machine qui déploit et sur celle qui reçoit

![bg right 100%](https://miro.medium.com/max/1400/1*MLPdeP8OQw14v7R79max6g.png)

---

### L'inventaire

* Écrit en format INI ou YAML.
* Inventaire dynamique (Proxmox, vSphere, Cloud...)

```ini
[web]
efficom-web01
efficom-web02 ansible_host="192.168.1.2"

[web:children] 
apache

[apache]
efficom-web03
```

---

#  Les playbooks

* Fichier dans lequel on écrit nos instructions.
* Écrit en YAML
  * Indentation = 2 espaces
* Composé de modules

---

# Aperçu d'un playbook

```yaml
- hosts: web
  tasks:
    - name: "Installation des paquets nécessaires"
      ansible.builtin.apt:
        name: 
          - nginx
        state: present
    - name: "Copie du fichier de configuration principale"
      ansible.builtin.template:
        src: files/nginx.conf
        dest: /etc/nginx/nginx.conf
    - ansible.builtin.service:
        name: nginx
        state: restarted
```

---

# Les modules

* Documentation sur https://docs.ansible.com/ansible/
* Possible d'ajouter des modules à Ansible
* Possible de coder ses propres modules

```yaml
- name: Copie du fichier de configuration principale
  ansible.builtin.template:
    src: files/nginx.conf
    dest: /etc/nginx/nginx.conf
```
---

# Executer un playbook

* Via la commande `ansible-playbook`
* Possible d'utiliser l'option **--diff** (-D)
  * Affiche les détails des changements
* Possible d'utiliser l'option **--check** (-C)
  * Exécute Ansible sans faire réellement les modificatoins sur la machine

```sh
ansible-playbook mon_playbook.yml
```

---

# Démonstration simple

Réutilisation de notre exemple : Installation d'un serveur web

Ce qu'on remarque : 

* L'idempotence:
  * Au premier lancement, "Changed" sur les tasks
  * Au second lancement, "OK" sur les tasks

---

# Plus de flexibilité : variabiliser

* Moins besoin de répéter
* Réutiliser le code

---

# Comment variabiliser

* Définir les variables dans `group_vars/` et `host_vars/`

```yaml
ma_variable: ma_valeur
```

---

# Démonstration avec variables

Réutilisation de notre exemple : Installation d'un serveur web

Ce qu'on remarque : 

* L'idempotence
  * Au premier lancement, "Changed" sur les tasks
  * Au second lancement, "OK" sur les tasks

---

# Les rôles

* Playbook variabilisé le plus possible 
  * Peut être intégré à n'importe quel projet
* Réutiliser le code
* Trouvable sur Ansible Galaxy (ou GitHub)

---

# Ansible VS Terraform

* Terraform : Provisioning d'infrastructure / Ansible : Configuration
* State
  * Stocke chaque action dans un fichier (state file)
  * Quand on applique une action, compare le fichier d'état et **joue uniquement les différences**
  * Notion inexistante sur Ansible, Ansible joue toutes les tâches et vérifie si un changement doit être fait.

---

### Ansible VS Terraform - Schéma

![bg 65%](https://1.cms.s81c.com/sites/default/files/2018-11-22/ans_terraform.png)

---

# Git

* Travailler en équipe
* Versioning, possible de rollback
* Chiffrement des données sensibles (Mots de passes, certificats...)
  * Ansible Vault 
  * Git Crypt
![bg 100% right:40%](https://cdn.freebiesupply.com/logos/thumbs/2x/git-logo.png)

---

# Ressources

![bg 100%](https://gitlab.com/uploads/-/system/project/avatar/19237246/xavkiLogoTransparent.png)
![bg 70%](https://www.ansiblebook.com/images/book-cover-3rd.png)
